package application.data;

import application.model.*;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;
import java.io.InputStream;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Collectors;


/**
 * Single access point to stored data. This singleton provides various static methods to access data.
 */
public class Data {
    private static Data ourInstance;
    private static final Logger LOG = Logger.getLogger(Data.class.getName());
    private static final String JAXB_CONTEXT_PATH = "application.jaxb";
    private static JAXBContext JAXB_CONTEXT;
    private static Unmarshaller UNMARSHALLER;
    private FlightsCollection flightsCollection;

    static {
        try {
            JAXB_CONTEXT = JAXBContext.newInstance(JAXB_CONTEXT_PATH);
            UNMARSHALLER = JAXB_CONTEXT.createUnmarshaller();
        } catch (JAXBException e) {
            e.printStackTrace();
        }
    }

    /**
     * Gets the one and only instance of this class (it is a singleton)
     *
     * @return
     */
    public static Data getInstance() {
        if (null == ourInstance) ourInstance = new Data();
        return ourInstance;
    }

    private Data() {
        InputStream inputStream = getClass().getClassLoader().getResourceAsStream("data/flights.xml");
        application.jaxb.FlightsCollection fc = (application.jaxb.FlightsCollection) unmarshallXML(inputStream);
        flightsCollection = new FlightsCollection(fc);
        System.out.println(String.format(
                "Flights data loaded for %d airports from %s to %s",
                flightsCollection.getNumAirports(),
                flightsCollection.getFrom(), flightsCollection.getTo()));
    }

    /**
     * Gets the initial date for which there are flight data
     *
     * @return
     */
    public LocalDate getFromDate() {
        return flightsCollection.getFrom().toLocalDate();
    }

    /**
     * Gets the final date for which there are flight data
     *
     * @return
     */
    public LocalDate getToDate() {
        return flightsCollection.getTo().toLocalDate();
    }

    /**
     * Gets the list of dates for which there are flight data
     *
     * @return
     */
    public List<LocalDate> getDates() {
        Map<LocalDate, FlightsCollection> flightsCollectionByDate = flightsCollection.getFlightsCollectionByDate();
        List<LocalDate> dates = new ArrayList<>(flightsCollectionByDate.size());
        dates.addAll(flightsCollectionByDate.keySet().stream().collect(Collectors.toList()));
        return dates;
    }

    /**
     * Gets the list of airports for which there are flight data
     *
     * @return
     */
    public List<Airport> getAirportList() {
        List<Airport> airports = new ArrayList<>();
        airports.addAll(flightsCollection.getAirports());
        return airports;
    }

    /**
     * Gets all the flight data for a single airport
     *
     * @param airport
     * @return
     */
    public AirportFlights getAirportFlights(Airport airport) {
        if (airport instanceof AENA_Airport) {
            return flightsCollection.getAirportFlights(AENA_Airport.getSimpleAirport((AENA_Airport) airport));
        } else return flightsCollection.getAirportFlights(airport);
    }

    /**
     * Gets the flight data for a single airport and a given date
     *
     * @param airport
     * @param date
            * @return
        */
        public AirportFlights getAirportFlights(Airport airport, LocalDate date) {
            return flightsCollection.getAirportFlights(airport).getAirportFlightsByDate().get(date);
        }

    private static Object unmarshallXML(InputStream inputStream) {
        try {
            Object object = UNMARSHALLER.unmarshal(inputStream);
            return object;
        } catch (JAXBException ex) {
            LOG.log(Level.SEVERE, "Exception unmarshalling XML", ex);
            return null;
        }
    }
}