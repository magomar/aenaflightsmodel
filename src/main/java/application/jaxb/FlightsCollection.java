package application.jaxb;

import javax.xml.bind.annotation.*;
import java.util.ArrayList;
import java.util.List;


/**
 * <p>Clase Java para anonymous complex type.
 * <p>
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * <p>
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="airport-flights" type="{}AirportFlights" maxOccurs="unbounded"/>
 *         &lt;element name="from" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="to" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
        "airportFlights",
        "from",
        "to"
})
@XmlRootElement(name = "flights-collection")
public class FlightsCollection {

    @XmlElement(name = "airport-flights", required = true)
    protected List<AirportFlights> airportFlights;
    @XmlElement(required = true)
    protected String from;
    @XmlElement(required = true)
    protected String to;

    /**
     * Gets the value of the airportFlights property.
     * <p>
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the airportFlights property.
     * <p>
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getAirportFlights().add(newItem);
     * </pre>
     * <p>
     * <p>
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link AirportFlights }
     */
    public List<AirportFlights> getAirportFlights() {
        if (airportFlights == null) {
            airportFlights = new ArrayList<AirportFlights>();
        }
        return this.airportFlights;
    }

    /**
     * Obtiene el valor de la propiedad from.
     *
     * @return possible object is
     * {@link String }
     */
    public String getFrom() {
        return from;
    }

    /**
     * Define el valor de la propiedad from.
     *
     * @param value allowed object is
     *              {@link String }
     */
    public void setFrom(String value) {
        this.from = value;
    }

    /**
     * Obtiene el valor de la propiedad to.
     *
     * @return possible object is
     * {@link String }
     */
    public String getTo() {
        return to;
    }

    /**
     * Define el valor de la propiedad to.
     *
     * @param value allowed object is
     *              {@link String }
     */
    public void setTo(String value) {
        this.to = value;
    }

}
