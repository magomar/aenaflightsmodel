package application.jaxb;

import javax.xml.bind.annotation.XmlRegistry;


/**
 * This object contains factory methods for each
 * Java content interface and Java element interface
 * generated in the dashboard.data.jaxb package.
 * <p>An ObjectFactory allows you to programatically
 * construct new instances of the Java representation
 * for XML content. The Java representation of XML
 * content can consist of schema derived interfaces
 * and classes representing the binding of schema
 * type definitions, element declarations and model
 * groups.  Factory methods for each of these are
 * provided in this class.
 */
@XmlRegistry
public class ObjectFactory {


    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: dashboard.data.jaxb
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link FlightsCollection }
     */
    public FlightsCollection createFlightsCollection() {
        return new FlightsCollection();
    }

    /**
     * Create an instance of {@link AirportFlights }
     */
    public AirportFlights createAirportFlights() {
        return new AirportFlights();
    }

    /**
     * Create an instance of {@link FlightsHistory }
     */
    public FlightsHistory createFlightsHistory() {
        return new FlightsHistory();
    }

    /**
     * Create an instance of {@link StatusEvent }
     */
    public StatusEvent createStatusEvent() {
        return new StatusEvent();
    }

    /**
     * Create an instance of {@link Flight }
     */
    public Flight createFlight() {
        return new Flight();
    }

    /**
     * Create an instance of {@link Airport }
     */
    public Airport createAirport() {
        return new Airport();
    }

    /**
     * Create an instance of {@link SharedFlight }
     */
    public SharedFlight createSharedFlight() {
        return new SharedFlight();
    }

}
