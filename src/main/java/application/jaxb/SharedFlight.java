package application.jaxb;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para SharedFlight complex type.
 * <p>
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * <p>
 * <pre>
 * &lt;complexType name="SharedFlight">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="flight-number" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="company" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SharedFlight", propOrder = {
        "flightNumber",
        "company"
})
public class SharedFlight {

    @XmlElement(name = "flight-number", required = true)
    protected String flightNumber;
    @XmlElement(required = true)
    protected String company;

    /**
     * Obtiene el valor de la propiedad flightNumber.
     *
     * @return possible object is
     * {@link String }
     */
    public String getFlightNumber() {
        return flightNumber;
    }

    /**
     * Define el valor de la propiedad flightNumber.
     *
     * @param value allowed object is
     *              {@link String }
     */
    public void setFlightNumber(String value) {
        this.flightNumber = value;
    }

    /**
     * Obtiene el valor de la propiedad company.
     *
     * @return possible object is
     * {@link String }
     */
    public String getCompany() {
        return company;
    }

    /**
     * Define el valor de la propiedad company.
     *
     * @param value allowed object is
     *              {@link String }
     */
    public void setCompany(String value) {
        this.company = value;
    }

}
