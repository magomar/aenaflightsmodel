package application.model;

/**
 * Created by Mario on 10/03/2015.
 */
public enum AENA_Airport implements Airport {
    LCG("A Coru�a"),
    MAD("Adolfo Suarez Madrid-Barajas"),
    ABC("Albacete"),
    AEI("Algeciras"),
    ALC("Alicante-Elche"),
    LEI("Almer�a"),
    OVD("Asturias"),
    BJZ("Badajoz"),
    BCN("Barcelona-El Prat"),
    BIO("Bilbao"),
    RGS("Burgos"),
    JCU("Ceuta"),
    ODB("C�rdoba"),
    VDE("El Hierro"),
    FUE("Fuerteventura"),
    GRO("Girona-Costa Brava"),
    LPA("Gran Canaria"),
    GRX("Granada-Ja�n F.G.L."),
    HSK("Huesca-Pirineos"),
    IBZ("Ibiza"),
    XRY("Jerez"),
    QGZ("La Gomera"),
    SPC("La Palma"),
    ACE("Lanzarote"),
    LEN("Le�n"),
    RJL("Logro�o-Agoncillo"),
    MCV("Madrid-Cuatro Vientos"),
    AGP("M�laga-Costa del Sol"),
    MLN("Melilla"),
    MAH("Menorca"),
    MJV("Murcia-San Javier"),
    PMI("Palma de Mallorca"),
    PNA("Pamplona"),
    REU("Reus"),
    QSA("Sabadell"),
    SLM("Salamanca"),
    EAS("San Sebasti�n"),
    SDR("Santander"),
    SCQ("Santiago"),
    SVQ("Sevilla"),
    SBO("Son Bonet"),
    TFN("Tenerife Norte"),
    TFS("Tenerife Sur"),
    VLC("Valencia"),
    VLL("Valladolid"),
    VGO("Vigo"),
    VIT("Vitoria"),
    ZAZ("Zaragoza");
    private final String fullName;

    private AENA_Airport(String fullName) {
        this.fullName = fullName;
    }

    @Override
    public application.jaxb.Airport getData() {
        application.jaxb.Airport data = new application.jaxb.Airport();
        data.setCode(name());
        data.setName(fullName);
        return data;
    }

    @Override
    public String getName() {
        return fullName;
    }

    @Override
    public String getCode() {
        return name();
    }

    @Override
    public String toString() {
        return fullName + " (" + name() + ')';
    }

    public static Airport getAirportByCode(String code) {
        try {
            AENA_Airport aenaAirport = AENA_Airport.valueOf(code);
            return new SimpleAirport(aenaAirport.getName(), aenaAirport.getCode());
        } catch (IllegalArgumentException e) {
            return null;
        }
    }

    public static Airport getSimpleAirport(AENA_Airport aenaAirport) {
        return new SimpleAirport(aenaAirport.getName(), aenaAirport.getCode());
    }
}
