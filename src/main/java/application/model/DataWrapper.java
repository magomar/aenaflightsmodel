package application.model;

/**
 * Created by Mario on 11/02/2015.
 */
public interface DataWrapper<D> {
    D getData();
}
