package application.model;

/**
 * The different states a flight can be in, relating to its schedulled vs executed status, and whether it is a departure
 * or an arrival
 */
public enum FlightStatusType {
    ESTIMATED_DEPARTURE("Salida prevista a las "),
    ESTIMATED_ARRIVAL("Llegada prevista a las "),
    DEPARTED("El vuelo ha despegado a las ", "Ha despegado a las "),
    ARRIVED("El vuelo ha aterrizado a las ", "Ha aterrizado a las ");

    static final FlightStatusType[] FLIGHT_STATUSES = FlightStatusType.values();
    private final String msg;
    private final String shortMsg;

    private FlightStatusType(String msg) {
        this.msg = msg;
        this.shortMsg = msg;
    }

    private FlightStatusType(String msg, String shortMsg) {
        this.msg = msg;
        this.shortMsg = shortMsg;
    }

    public static FlightStatusType getFlightStatusTypeFromMessage(String msg) {
        for (FlightStatusType flightStatu : FLIGHT_STATUSES) {
            if (msg.startsWith(flightStatu.msg)) return flightStatu;
        }
        return null;
    }

    public String getMsg() {
        return msg;
    }

    public String getShortMsg() {
        return shortMsg;
    }
}
/*
El vuelo ha despegado a las 20:41
El vuelo ha aterrizado a las 22:00
Salida prevista a las 06:05
Llegada prevista a las 07:00
*/