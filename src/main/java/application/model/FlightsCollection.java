package application.model;

import application.util.DateTimeUtils;
import javafx.beans.binding.NumberBinding;
import javafx.beans.property.IntegerProperty;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableMap;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.Collection;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;
import java.util.stream.Collectors;

/**
 * Collection of flights for multiple airports. Flights are organized into various collections, one for each
 * airport; the flights corresponding to a single airport are specified as an object of type {@link AirportFlights}.
 */
public class FlightsCollection implements DataWrapper<application.jaxb.FlightsCollection> {

    private ObservableMap<Airport, AirportFlights> airportFlights = FXCollections.observableHashMap();
    private final IntegerProperty numAirports = new SimpleIntegerProperty(0);
    private final IntegerProperty numFlights = new SimpleIntegerProperty(0);
    private final IntegerProperty numNationalFlights = new SimpleIntegerProperty(0);
    private final IntegerProperty numInternationalFlights = new SimpleIntegerProperty(0);
    private NumberBinding numFlightsBinding = new SimpleIntegerProperty().add(0);
    private NumberBinding numNationalFlightsBinding = new SimpleIntegerProperty().add(0);
    private NumberBinding numInternationalFlightsBinding = new SimpleIntegerProperty().add(0);
    private ObjectProperty<LocalDateTime> from = new SimpleObjectProperty<>(LocalDateTime.MAX);
    private ObjectProperty<LocalDateTime> to = new SimpleObjectProperty<>(LocalDateTime.MIN);

    public FlightsCollection(application.jaxb.FlightsCollection flightsCollection) {
        for (application.jaxb.AirportFlights airportFlights : flightsCollection.getAirportFlights()) {
            putAirportFlights(new SimpleAirport(airportFlights.getAirport()), new AirportFlights(airportFlights));
        }
    }

    public FlightsCollection() {

    }

    // TODO no need to return an object except for the #updateFlights method, so try to change that method
    public AirportFlights updateAirportFlights(AirportFlights airportFlights) {
        Airport airport = airportFlights.getAirport();
        if (!this.airportFlights.containsKey(airport)) {
            putAirportFlights(airport, airportFlights);
        } else {
            this.airportFlights.get(airport).updateFlights(airportFlights);
        }
        return airportFlights;
    }

    private void putAirportFlights(Airport airport, AirportFlights airportFlights) {
        this.airportFlights.put(airport, airportFlights);
        numFlightsBinding = numFlightsBinding.add(airportFlights.numFlightsProperty());
        numNationalFlightsBinding = numNationalFlightsBinding.add(airportFlights.numNationalFlightsProperty());
        numInternationalFlightsBinding = numInternationalFlightsBinding.add(airportFlights.numInternationalFlightsProperty());
        numFlights.bind(numFlightsBinding);
        numNationalFlights.bind(numNationalFlightsBinding);
        numInternationalFlights.bind(numInternationalFlightsBinding);
        numAirports.set(this.airportFlights.size());
        if (airportFlights.getFrom().isBefore(from.get()))
            from.set(airportFlights.getFrom());
        airportFlights.fromProperty().addListener((observable, oldValue, newValue) -> {
            if (newValue.isBefore(from.get()))
                from.set(newValue);
        });
        if (airportFlights.getTo().isAfter(to.get()))
            to.set(airportFlights.getTo());
        airportFlights.toProperty().addListener((observable, oldValue, newValue) -> {
            if (newValue.isAfter(to.get()))
                to.set(newValue);
        });

    }

    @Override
    public application.jaxb.FlightsCollection getData() {
        application.jaxb.FlightsCollection data = new application.jaxb.FlightsCollection();
        data.setFrom(DateTimeUtils.format(from.get()));
        data.setTo(DateTimeUtils.format(to.get()));
        data.getAirportFlights().addAll(
                this.airportFlights.values()
                        .stream()
                        .map(AirportFlights::getData)
                        .collect(Collectors.toList()));
        return data;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder("FlightsCollection\n");
        sb.append("- From: ").append(DateTimeUtils.format(from.get())).append('\n');
        sb.append("- To: ").append(DateTimeUtils.format(to.get())).append('\n');
        sb.append("Airports (numflights):\n");
        for (AirportFlights airportFlights : this.airportFlights.values()) {
            sb.append(airportFlights.getAirport().getCode());
            sb.append("(").append(airportFlights.getNumFlights()).append(')');
        }
        sb.append("\n");
        return sb.toString();
    }

    public AirportFlights getAirportFlights(Airport airport) {
        return airportFlights.get(airport);
    }

    public Collection<AirportFlights> getAirportFlights() {
        return airportFlights.values();
    }

    public Set<Airport> getAirports() {
        return airportFlights.keySet();
    }

    public int getNumAirports() {
        return numAirports.get();
    }

    public IntegerProperty numAirportsProperty() {
        return numAirports;
    }

    public int getNumFlights() {
        return numFlights.get();
    }

    public IntegerProperty numFlightsProperty() {
        return numFlights;
    }

    public LocalDateTime getFrom() {
        return from.get();
    }

    public ObjectProperty<LocalDateTime> fromProperty() {
        return from;
    }

    public LocalDateTime getTo() {
        return to.get();
    }

    public ObjectProperty<LocalDateTime> toProperty() {
        return to;
    }

    public int getNumNationalFlights() {
        return numNationalFlights.get();
    }

    public IntegerProperty numNationalFlightsProperty() {
        return numNationalFlights;
    }

    public int getNumInternationalFlights() {
        return numInternationalFlights.get();
    }

    public IntegerProperty numInternationalFlightsProperty() {
        return numInternationalFlights;
    }

    public Map<LocalDate, FlightsCollection> getFlightsCollectionByDate() {
        Map<LocalDate, FlightsCollection> flightsCollectionByDate = new TreeMap<>();
        LocalDate dateFrom = from.get().toLocalDate();
        LocalDate dateTo = to.get().toLocalDate();
        LocalDate dateBetween = LocalDate.from(dateFrom);
        while (dateBetween.isBefore(dateTo) || dateBetween.isEqual(dateTo)) {
            FlightsCollection fc = new FlightsCollection();
            flightsCollectionByDate.put(dateBetween, fc);
            dateBetween = dateBetween.plusDays(1);
        }
        for (AirportFlights airportFlights : getAirportFlights()) {
            Airport airport = airportFlights.getAirport();
            Map<LocalDate, FlightsHistory> departuresByDate = airportFlights.getDepartures().getFlightsHistoryByDate();
            Map<LocalDate, FlightsHistory> arrivalsByDate = airportFlights.getArrivals().getFlightsHistoryByDate();
            for (LocalDate date : flightsCollectionByDate.keySet()) {
                AirportFlights af = new AirportFlights(airport);
                if (arrivalsByDate.containsKey(date))
                    af.getArrivals().addFlights(arrivalsByDate.get(date));
                if (departuresByDate.containsKey(date))
                    af.getDepartures().addFlights(departuresByDate.get(date));
                flightsCollectionByDate.get(date).updateAirportFlights(af);
            }
        }
        return flightsCollectionByDate;
    }

}
