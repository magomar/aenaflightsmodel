package application.model;

/**
 *
 * Any airport described a name and an airport code.
 */
public class SimpleAirport implements Airport {
    private String name;
    private String code;

    public SimpleAirport(String name, String code) {
        this.name = name;
        this.code = code;
    }

    public SimpleAirport(application.jaxb.Airport airport) {
        name = airport.getName();
        code = airport.getCode();
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public String getCode() {
        return code;
    }


    @Override
    public String toString() {
        return name + " (" + code + ')';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        SimpleAirport airport = (SimpleAirport) o;

        if (!code.equals(airport.code)) return false;

        return true;
    }

    @Override
    public int hashCode() {
        return code.hashCode();
    }

    public application.jaxb.Airport getData() {
        application.jaxb.Airport data = new application.jaxb.Airport();
        data.setCode(code);
        data.setName(name);
        return data;
    }
}
