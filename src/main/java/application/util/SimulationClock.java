package application.util;


import application.model.ClockListener;
import javafx.beans.property.DoubleProperty;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

/**
 * Created by Mario on 15/03/2015.
 */
public class SimulationClock {
    private final LocalDateTime from;
    private final LocalDateTime to;
    private final DoubleProperty speed;
    private LocalDateTime dateTime;
    private ScheduledThreadPoolExecutor scheduledThreadPoolExecutor;
    private boolean isPaused = true;
    private List<ClockListener> listeners = new ArrayList<>();

    public SimulationClock(LocalDateTime from, LocalDateTime to, DoubleProperty speed) {
        this.from = from;
        this.to = to;
        this.speed = speed;
        dateTime = LocalDateTime.from(from);
    }

    public synchronized void addListener(ClockListener listener) {
        listeners.add(listener);
    }

    public synchronized void removeListener(ClockListener listener) {
        listeners.remove(listener);
    }

    public void play() {
        if (isPaused) {
            int cores = Runtime.getRuntime().availableProcessors();
            scheduledThreadPoolExecutor = (ScheduledThreadPoolExecutor) Executors.newScheduledThreadPool(cores);
            scheduledThreadPoolExecutor.scheduleAtFixedRate(() -> {
                dateTime = dateTime.plusMinutes(Math.max(1L, (long) speed.get()));
                notifyListeners();
            }, 0, 1L, TimeUnit.SECONDS);
            isPaused = false;
        }
    }

    private void notifyListeners() {
        for (ClockListener listener : listeners) {
            listener.update(dateTime);
        }
    }

    public void pause() {
        if (!isPaused) {
            scheduledThreadPoolExecutor.shutdown();
            try {
                scheduledThreadPoolExecutor.awaitTermination(5, TimeUnit.MINUTES);
                isPaused = true;
            } catch (InterruptedException e) {
                e.printStackTrace();
            } finally {

            }
        }
    }

}
