package application.data;

import application.model.AENA_Airport;
import application.model.Airport;
import application.model.AirportFlights;
import junit.framework.TestCase;

import java.time.LocalDate;

/**
 * Created by Mario on 26/04/2015.
 */
public class DataTest extends TestCase {
    Data data;

    public void setUp() throws Exception {
        super.setUp();
        data = Data.getInstance();
    }

    public void tearDown() throws Exception {
        data = null;
    }

    public void testGetFromDate() throws Exception {
        assertNotNull(data.getFromDate());
    }

    public void testGetToDate() throws Exception {
        assertNotNull(data.getToDate());
    }

    public void testGetDates() throws Exception {
        assertNotNull(data.getDates());
    }

    public void testGetAirportList() throws Exception {
        assertNotNull(data.getAirportList());
    }

    public void testGetAirportFlights() throws Exception {
        Airport airport = data.getAirportList().get(0);
        AirportFlights airportFlights = data.getAirportFlights(airport);
        assertNotNull(airportFlights);
        airportFlights = data.getAirportFlights(AENA_Airport.VLC);
        assertNotNull(airportFlights);
    }

    public void testGetAirportFlights1() throws Exception {
        AirportFlights airportFlights = data.getAirportFlights(data.getAirportList().get(0), LocalDate.from(data.getFromDate()));
        assertNotNull(airportFlights);
    }
}